# Zadanie 4. Arytmetyka Peano

Uzupełnij plik `peano.pl` zgodnie z instrukcjami w:

<details><summary>[arytmetyka peano](https://swish.swi-prolog.org/?code=https://gitlab.com/agh-courses/2020-2021/prolog/lab-02-efekty-uboczne-i-arytmetyka/-/raw/master/assignments/04_peano/instrukcje.swinb)</summary>
<ul>
<li> [alternatywny link 1](http://lpsdemo.interprolog.com/?code=https://gitlab.com/agh-courses/2020-2021/prolog/lab-02-efekty-uboczne-i-arytmetyka/-/raw/master/assignments/04_peano/instrukcje.swinb)
<li> [alternatywny link 2](http://cplint.ml.unife.it?code=https://gitlab.com/agh-courses/2020-2021/prolog/lab-02-efekty-uboczne-i-arytmetyka/-/raw/master/assignments/04_peano/instrukcje.swinb)
</details>
