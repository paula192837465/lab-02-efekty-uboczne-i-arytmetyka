# Zadanie 1. Krewni 2.0

Uzupełnij plik `krewni.pl` zgodnie z instrukcjami w:

<details><summary>[krewni 2.0](https://swish.swi-prolog.org/?code=https://gitlab.com/agh-courses/2020-2021/prolog/lab-02-efekty-uboczne-i-arytmetyka/-/raw/master/assignments/01_krewni/instrukcje.swinb)</summary>
<ul>
<li> [alternatywny link 1](http://lpsdemo.interprolog.com/?code=https://gitlab.com/agh-courses/2020-2021/prolog/lab-02-efekty-uboczne-i-arytmetyka/-/raw/master/assignments/01_krewni/instrukcje.swinb)
<li> [alternatywny link 2](http://cplint.ml.unife.it?code=https://gitlab.com/agh-courses/2020-2021/prolog/lab-02-efekty-uboczne-i-arytmetyka/-/raw/master/assignments/01_krewni/instrukcje.swinb)
</details>
