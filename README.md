# Lab 02. Efekty uboczne i obliczenia

Celem tego laboratorium jest zapoznanie się z elementami Prologa wykraczającymi poza zwykłą rezolucję i logiczne operacje.

## Notatniki

<details><summary>[efekty uboczne](https://swish.swi-prolog.org/?code=https://gitlab.com/agh-courses/2020-2021/prolog/lab-02-efekty-uboczne-i-arytmetyka/-/raw/master/notebooks/03_efekty_uboczne.swinb)</summary>
<ul>
<li> [alternatywny link 1](http://lpsdemo.interprolog.com/?code=https://gitlab.com/agh-courses/2020-2021/prolog/lab-02-efekty-uboczne-i-arytmetyka/-/raw/master/notebooks/03_efekty_uboczne.swinb)
<li> [alternatywny link 2](http://cplint.ml.unife.it?code=https://gitlab.com/agh-courses/2020-2021/prolog/lab-02-efekty-uboczne-i-arytmetyka/-/raw/master/notebooks/03_efekty_uboczne.swinb)
</details>
<details><summary>[arytmetyka](https://swish.swi-prolog.org/?code=https://gitlab.com/agh-courses/2020-2021/prolog/lab-02-efekty-uboczne-i-arytmetyka/-/raw/master/notebooks/04_obliczenia.swinb)</summary>
<ul>
<li> [alternatywny link 1](http://lpsdemo.interprolog.com/?code=https://gitlab.com/agh-courses/2020-2021/prolog/lab-02-efekty-uboczne-i-arytmetyka/-/raw/master/notebooks/04_obliczenia.swinb)
<li> [alternatywny link 2](http://cplint.ml.unife.it?code=https://gitlab.com/agh-courses/2020-2021/prolog/lab-02-efekty-uboczne-i-arytmetyka/-/raw/master/notebooks/04_obliczenia.swinb)
</details>

## Zadania

Proszę uzupełnić odpowiednie pliki w katalogu `assignments`.

## GitLab Setup 

* [ ] Upewnij się, że masz **prywatną** grupę 
  * [jak stworzyć grupę?](https://docs.gitlab.com/ee/user/group/#create-a-group)
* [ ] Dodaj @bobot-is-a-bot jako nowego członka grupa (rola: **maintainer**)
  * [jak dodać członka do grupy?](https://docs.gitlab.com/ee/user/group/#add-users-to-a-group)
* [ ] Stwórz fork tego projekty wewnątrz swojej **prywatnej** grupy
  * [jak stworzyć forka?](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork)

## Jak wysyłać rozwiązania

* [ ] Zklonuj swojego forka:
    ```bash 
    git clone <repository url>
    ```
* [ ] Rozwiąż zadania:
    * Pamiętaj, żeby modyfikować pliki jedynie w miejscach komentarzy `TODO` lub `@tbd`.
* [ ] Zacommituj swoje zmiany
    ```bash
    git add <path to the changed files>
    git commit -m <commit message>
    ```
* [ ] Wypchnij zmiany na gałąź **master**
    ```bash
    git push -u origin master
    ```

Resztą zajmie się Bobot. Możesz sprawdzić plik `GRADE.md`, żeby zobaczyć swoje wyniki. Plik `GRADE.md` aktualizowany jest w odstępach 15 min.

PS Bobot nie zna niestety języka polskiego.

